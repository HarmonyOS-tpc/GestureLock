# demo

<img src="art/demo.gif" width="40%"/>

# how to user?

```
dependencies{
    implementation 'io.openharmony.tpc.thirdlib:GestureLock:1.0.1'
}
```

```xml
<com.sevenheaven.gesturelock.GestureLock
        ohos:margin="20vp"
        GestureLock:line_width="2vp"
        GestureLock:line_normal_color="#ffffaa"
        ohos:id="$+id:gesturelock"
        ohos:height="match_parent"
        ohos:width="match_parent"/>
```

```java
gestureLock.setAdapter(new GestureLock.GestureLockAdapter() {
            @Override
            public int getDepth() {
                return 3;
                //解锁点属性返回3为3X3，返回4为4X4
            }

            @Override
            public int[] getCorrectGestures() {
                return int[]{0,1,2};
                //解锁正确手势
            }

            @Override
            public int getUnmatchedBoundary() {
                return 3;
                //最大错误数
            }

            @Override
            public int getBlockGapSize() {
                return 30;
                //解锁点间距PX
            }

            @Override
            public GestureLockView getGestureLockViewInstance(Context context, int position) {
                return new MyStyleLockView(gestureLock);
                //解锁点样式
            }
        });
        gestureLock.setOnGestureEventListener(new GestureLock.OnGestureEventListener() {
            @Override
            public void onBlockSelected(int position) {
                 //结果的解锁点
            }

            @Override
            public void onGestureEvent(boolean matched,int errorTime) {
                //matched是否正确 ，errorTime错误次数
            }

            @Override
            public void onUnmatchedExceedBoundary() {
                //错误次数超过限制
            }

            @Override
            public void onGesturesFinish(int[] gestures) {
                //返回的解锁手势
            }
        });
```

# API:
## class GestrueLock
**public void setAdapter(GestureLockAdapter adapter)**
- description: set gesture lock attr values with adapter

**public void setUnmatchedBoundary(int unmatchedBoundary)**
- description: set unmatched boundary

**public int getUnmatchedBoundary()**
- description: get unmatched boundary

**public void setCorrectGestures(int[] gesturesContainer)**
- description: set correct gestures

**public int[] getCorrectGestures()**
- description: get correct gestures

**public void notifyDataChanged()**
- description: refresh data

**public void setEditMode(int mode)**
- description: set edit mode

**public int getEditMode()**
- description: get edit mode

**public void setTouchable(boolean touchable)**
- description: set touchable

**public void resetUnmatchedCount()**
- description: reset unmatched count

**public void setOnGestureEventListener(OnGestureEventListener onGestureEventListener)**
- description: set gesture event listener

**public void clear()**
- description: clear gestrues

## abstract class GestureLockView
**public void setLockerState(LockerState state)**
- description: set locker state

**public LockerState getLockerState()**
- description: get locker state

**public void setArrow(int arrow)**
- description: set arrow angle

**public int getArrow()**
- description: get arrow angle

# AttrSet:

|name|format|description|
|:---:|:---:|:---:|
| line_width | dimension | set line width
| line_normal_color | color | set line default color
| line_error_color | color | set line error color


