package com.sevenheaven.gesturelock;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * Abstract class for user to customize the GestureLock block style
 */
public abstract class GestureLockView {
    private static final boolean DEBUG = false;

    private int mWidth, mHeight;
    private int mCenterX, mCenterY;

    private Paint mPaint;

    public enum LockerState {
        LOCKER_STATE_NORMAL,
        LOCKER_STATE_ERROR,
        LOCKER_STATE_SELECTED
    }

    private LockerState mState = LockerState.LOCKER_STATE_NORMAL;

    private int errorArrow = -1;

    private GestureLock gestureLock;

    public GestureLockView(GestureLock gestureLock) {
        this.gestureLock = gestureLock;
    }

    void draw(Canvas canvas, int width, int height) {
        onSizeChanged(width, height);
        if (DEBUG) {
            if (mPaint == null) {
                mPaint = new Paint();
                mPaint.setAntiAlias(true);
            }
            mPaint.setColor(new Color(0xFFFF0000));
            mPaint.setTextSize(20);
            canvas.drawText(mPaint, "avav", 0, 20);
        }

        doDraw(mState, canvas);

        if (errorArrow != -1) {
            canvas.save();
            canvas.rotate(errorArrow, mCenterX, mCenterY);
            doArrowDraw(mState, canvas);

            canvas.restore();
        }
    }

    protected void onSizeChanged(int w, int h) {
        mWidth = w;
        mHeight = h;

        mCenterX = mWidth / 2;
        mCenterY = mHeight / 2;
    }

    public void setLockerState(LockerState state) {
        mState = state;
        gestureLock.invalidate();
    }

    public LockerState getLockerState() {
        return mState;
    }

    public void setArrow(int arrow) {
        errorArrow = arrow;

        gestureLock.invalidate();
    }

    public int getArrow() {
        return errorArrow;
    }

    /**
     * override this method to do GestureLock block drawing work
     * @param state current state of this GustureLock block
     * @param canvas the canvas from GestureLock
     */
    protected abstract void doDraw(LockerState state, Canvas canvas);

    /**
     * override this method to do Arrow drawing, you should notice
     * @param state the state
     * @param canvas the canvas from GestureLock
     */
    protected abstract void doArrowDraw(LockerState state, Canvas canvas);
}
