package com.sevenheaven.gesturelock.entry.slice;

import com.sevenheaven.gesturelock.GestureLock;
import com.sevenheaven.gesturelock.GestureLockView;
import com.sevenheaven.gesturelock.MyStyleLockView;

import com.sevenheaven.gesturelock.entry.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class MainAbilitySlice extends AbilitySlice {
    private GestureLock gestureLock;
    private Text edit;
    private Text error;
    private int[] gestures = new int[] {0, 1, 2};

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        gestureLock = (GestureLock) findComponentById(ResourceTable.Id_gesturelock);
        edit = (Text) findComponentById(ResourceTable.Id_edit);
        error = (Text) findComponentById(ResourceTable.Id_error);
        edit.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (gestureLock.getEditMode() == GestureLock.MODE_NORMAL) {
                            gestureLock.setEditMode(GestureLock.MODE_EDIT);
                            edit.setText("编辑中");
                        }
                    }
                });
        gestureLock.setAdapter(
                new GestureLock.GestureLockAdapter() {
                    @Override
                    public int getDepth() {
                        return 3;
                    }

                    @Override
                    public int[] getCorrectGestures() {
                        return gestures;
                    }

                    @Override
                    public int getUnmatchedBoundary() {
                        return 3;
                    }

                    @Override
                    public int getBlockGapSize() {
                        return 30;
                    }

                    @Override
                    public GestureLockView getGestureLockViewInstance(Context context, int position) {
                        return new MyStyleLockView(gestureLock);
                    }
                });
        gestureLock.setOnGestureEventListener(
                new GestureLock.OnGestureEventListener() {
                    @Override
                    public void onBlockSelected(int position) {}

                    @Override
                    public void onGestureEvent(boolean matched, int errorTime) {
                        if (matched) {
                            error.setText("正确");
                            error.setTextColor(Color.GREEN);
                        } else {
                            error.setText("错误次数:" + errorTime);
                            if (errorTime != 0) {
                                error.setTextColor(Color.RED);
                            } else {
                                error.setTextColor(Color.GRAY);
                            }
                        }
                    }

                    @Override
                    public void onUnmatchedExceedBoundary() {
                        error.setText("错误次数超过限制");
                    }

                    @Override
                    public void onGesturesFinish(int[] gestures) {
                        if (gestureLock.getEditMode() == GestureLock.MODE_EDIT) {
                            edit.setText("解锁中");
                            gestureLock.setCorrectGestures(gestures);
                            gestureLock.setEditMode(GestureLock.MODE_NORMAL);
                        }
                    }
                });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
